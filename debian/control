Source: kdesu
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               dh-sequence-kf5,
               dh-sequence-pkgkde-symbolshelper,
               doxygen,
               extra-cmake-modules (>= 5.100.0~),
               libkf5config-dev (>= 5.100.0~),
               libkf5coreaddons-dev (>= 5.100.0~),
               libkf5i18n-dev (>= 5.100.0~),
               libkf5pty-dev (>= 5.100.0~),
               libkf5service-dev (>= 5.90.0~),
               libqt5sql5-sqlite,
               qtbase5-dev (>= 5.15.2~),
               qttools5-dev,
               qttools5-dev-tools (>= 5.4),
Standards-Version: 4.6.1
Homepage: https://invent.kde.org/frameworks/kdesu
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kdesu
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kdesu.git
Rules-Requires-Root: no

Package: libkf5su-bin
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: runtime files for kdesu
 Framework to handle super user actions.
 .
 Contains runtime files for kdesu.

Package: libkf5su-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: translation files for kdesu
 Framework to handle super user actions
 .
 Contains translation files for kdesu.

Package: libkf5su-dev
Section: libdevel
Architecture: any
Depends: libkf5pty-dev (>= 5.100.0~),
         libkf5su5 (= ${binary:Version}),
         ${misc:Depends},
Recommends: libkf5su-doc (= ${source:Version})
Description: development files for kdesu
 Framework to handle super user actions.
 .
 Contains development files for kdesu.

Package: libkf5su-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: runtime files for kdesu (documentation)
 Framework to handle super user actions.
 .
 This package contains the qch documentation files.
Section: doc

Package: libkf5su5
Architecture: any
Multi-Arch: same
Depends: libkf5su-data (= ${source:Version}),
         sudo,
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: libkf5su-bin (= ${binary:Version})
Description: Integration with su for privilege escalation
 Framework to handle super user actions
